

-- this is a helper function - not meant to be used directly from sql
DROP Function getIntellgiendCharDistHelper2;
CREATE OR REPLACE Function getIntellgiendCharDistHelper2
   ( search IN varchar2, find IN varchar2, lastFoundParam IN INTEGER )
   RETURN number
IS
   dRet number;
   foundCount integer;
   charCount integer;
   lastFoundBuf integer;
   lastFound integer;
   charAt varchar2(500);
   numBuf integer;
   allowedExtraLetters integer;
BEGIN
  dRet := 0;
  foundCount := 0;
  lastFound := lastFoundParam;
  allowedExtraLetters := 5;
  charCount := length(find);

  for i in 1 .. charCount
  loop
    charAt := substr(find, i, 1);
    lastFoundBuf := instr(search, charAt, lastFound);
    
    if (lastFoundBuf - lastFound) >= allowedExtraLetters then
				lastFound := lastFound + 1;
				continue;
    end if;    

--		if (lastFoundBuf > lastFound AND lastFoundBuf > 0) then
		if (lastFoundBuf > 0) then
				lastFound := lastFoundBuf;
				foundCount := foundCount + 1;
		end if;
    
  end loop;


  numBuf := length(search);
  dRet := foundCount/numBuf;

RETURN dRet;

EXCEPTION
WHEN OTHERS THEN
   raise_application_error(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
END;
/


-- this is a helper function - not meant to be used directly from sql
CREATE OR REPLACE Function getIntellgiendCharDistHelper1
   ( search IN varchar2, find IN varchar2)
   RETURN number
IS
   dRet number;
   dRetBuf number;
   searchLength integer;
BEGIN
   dRet := 0;

		searchLength := length(search);
		for i in 1 .. searchLength
    loop
			dRetBuf := getIntellgiendCharDistHelper2(search, find, i);
			if dRetBuf > dRet then
				dRet := dRetBuf;
			end if;
		end loop;


RETURN dRet;

EXCEPTION
WHEN OTHERS THEN
   raise_application_error(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
END;
/




-- this is the main function to be used in SQL
CREATE OR REPLACE Function getIntellgiendWordDistance
   ( orgString IN varchar2, cmpString IN varchar2, caseInsensitive IN  INTEGER)
   RETURN number
IS
		dRet number;
		dRetBuf number;
		orgLc varchar2(500);
		cmpLc varchar2(500);
		sLonger varchar2(500);
		sShorter varchar2(500);
    iBuf integer;
    iBuf2 integer;
BEGIN

		dRet := 0;
		dRetBuf := 0;
		orgLc := NULL;
		cmpLc := NULL;
		sLonger := NULL;
		sShorter := NULL;


		if caseInsensitive = 1 then
			orgLc := lower(orgString);
			cmpLc := lower(cmpString);
		else
			orgLc := orgString;
			cmpLc := cmpString;
		end if;

    iBuf := length(orgLc);		
    iBuf2 := length(cmpLc);		
		if iBuf >= iBuf2 then
			sLonger := orgLc;
			sShorter := cmpLc;
		else
			sLonger := cmpLc;
			sShorter := orgLc;
		end if;

			dRet := getIntellgiendCharDistHelper1(sLonger, sShorter);
		

		return dRet;

EXCEPTION
WHEN OTHERS THEN
   raise_application_error(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
END;
/


-- this is the main function to be used in SQL
CREATE OR REPLACE Function getIntellgiendSentenceDistance
   ( orgSentence IN varchar2, cmpSentence IN varchar2, caseInsensitive IN  INTEGER)
   RETURN number
IS
    TYPE list_of_average_t IS TABLE OF number;
		dRet number;
		dRetBuf number;
		dRetTot number;
		orgLc varchar2(500);
		cmpLc varchar2(500);
		searchWord varchar2(500);
		bUseWeightAverage number;
    bestMatches list_of_average_t := list_of_average_t ();
    orgWords apex_application_global.vc_arr2;
    cmpWords apex_application_global.vc_arr2;
		s varchar2(500);
		bestMatchCounter number;
BEGIN

		dRet := 0;
		dRetBuf := 0;
		dRetTot := 0;
		orgLc := NULL;
		cmpLc := NULL;
    bUseWeightAverage := 1;
    bestMatchCounter := 0;
    searchWord := NULL;

		if caseInsensitive = 1 then
			orgLc := lower(orgSentence);
			cmpLc := lower(cmpSentence);
		else
			orgLc := orgSentence;
			cmpLc := cmpSentence;
		end if;

		orgWords := apex_util.string_to_table(orgLc, ' ');
		cmpWords := apex_util.string_to_table(cmpLc, ' ');

    for i in 1 .. orgWords.count
    loop
      s := trim(orgWords(i));
      dRet := 0;

      for ii in 1 .. cmpWords.count
      loop
				dRetBuf := getIntellgiendWordDistance(s, trim(cmpWords(ii)), caseInsensitive);
				if dRetBuf > dRet then
					searchWord := cmpWords(ii);
					dRet := dRetBuf;
				end if;
      end loop;

			if bUseWeightAverage = 1 then

				for iii in 1 .. length(s) 
        loop
          bestMatchCounter := bestMatchCounter + 1;
          bestMatches.EXTEND;
					bestMatches(bestMatchCounter) := dRet;
				end loop;

        if searchWord IS NOT NULL THEN

  				for iiii in 1 .. length(searchWord) 
            loop
            bestMatchCounter := bestMatchCounter + 1;
            bestMatches.EXTEND;
            bestMatches(bestMatchCounter) := dRet;
          end loop;
        end if;
        
			else
        bestMatches.EXTEND;
				bestMatches(1) := dRet;
			end if;

    end loop;

		for i in 1 .. bestMatches.count
    loop
			  dRetTot := dRetTot + bestMatches(i);
		end loop;

		dRet := (dRetTot / (bestMatches.count));
--		dRet := bestMatches.count;
--		dRet := dRetTot;


		return dRet;


EXCEPTION
WHEN OTHERS THEN
   raise_application_error(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
END;
/

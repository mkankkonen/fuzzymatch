package com.intelligend.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * Hello world!
 *
 */
public class FuzzyMatch 
{

	private int allowedExtraLetters = 5;
	private boolean bSentence = false;
	private boolean bUseWeightAvarage = true;
	private boolean bPrint = false;

	
	public FuzzyMatch(){
	}

	public void setAllowedExtraLetters(int allowedExtraLetters){
		this.allowedExtraLetters = allowedExtraLetters;
	}
	
	public void setUseWeightAvarage(boolean weightAvarage){
		this.bUseWeightAvarage = weightAvarage;
	}
	
	public void setPrintCmpStat(boolean print){
		bPrint = print;
	}
	
	private double getIntellgiendCharDistance(String search, String find, int lastFound) throws Exception {
		double dRet = 0;
		int foundCount = 0;
		
		for(int i = 0; i < find.length(); i++){
			int lastFoundBuf = search.indexOf(find.charAt(i), lastFound);

			
			if((lastFoundBuf - lastFound) >= allowedExtraLetters){
				lastFound++;
				continue;
			}
			
			if(lastFoundBuf > lastFound){
				lastFound = lastFoundBuf;
				foundCount++;
			}
		}

		dRet = (double)foundCount/search.length();
		return dRet;
	}
	
	
	private double getIntellgiendCharDistance(String search, String find) throws Exception {
		double dRet = 0;
		double dRetBuf = 0;
		
		for(int i = -1; i < search.length(); i++){
			dRetBuf = getIntellgiendCharDistance(search, find, i);
			if(dRetBuf > dRet){
				dRet = dRetBuf;
			}
		}

		return dRet;
	}

	
	public double getIntelligendWordDistance(String orgString, String cmpString, boolean caseInsensitive, int allowedNonceseLetters) throws Exception {
		this.allowedExtraLetters = allowedNonceseLetters;
		return this.getIntelligendWordDistance(orgString, cmpString, caseInsensitive);
	}
	
	public double getIntelligendWordDistance(String orgString, String cmpString, boolean caseInsensitive) throws Exception {
		double dRet = 0;
		String orgLc = null;
		String cmpLc = null;
		String sLonger = null;
		String sShorter = null;
		if(!bSentence && bPrint){
			System.out.println("word: " + orgString + " ~= " + cmpString);
		}
		if(caseInsensitive){
			orgLc = orgString.toLowerCase();
			cmpLc = cmpString.toLowerCase();
		}else{
			orgLc = orgString;
			cmpLc = cmpString;
		}
		
		if(orgLc.length() >= cmpLc.length()){
			sLonger = orgLc;
			sShorter = cmpLc;
		}else{
			sLonger = cmpLc;
			sShorter = orgLc;
		}

		if(sLonger.indexOf(sShorter) != -1){
			dRet = (double)sShorter.length()/sLonger.length();
		}else{
			dRet = getIntellgiendCharDistance(sLonger, sShorter);
		}
		

		if(!bSentence && bPrint){
			System.out.println("  distance: " + dRet);
		}
		return dRet;
	}

	public double getIntelligendSentenceDistance(String orgString, String cmpString, boolean caseInsensitive, int allowedNonceseLetters) throws Exception {
		this.allowedExtraLetters = allowedNonceseLetters;
		return this.getIntelligendSentenceDistance(orgString, cmpString, caseInsensitive);
	}
	
	public double getIntelligendSentenceDistance(String orgSentence, String cmpSentence, boolean caseInsensitive) throws Exception {
		double dRet = 0;
		double dRetBuf = 0;
		double dRetTot = 0;
		bSentence = true;
		if(bPrint){
			System.out.println("sentence: " + orgSentence + " ~= " + cmpSentence);
		}
		List<Double> bestMatches = new ArrayList<Double>(); 
		String orgLc = null;
		String cmpLc = null;
		List<String> orgWords = null;
		List<String> cmpWords = null;
		
		if(caseInsensitive){
			orgLc = orgSentence.toLowerCase();
			cmpLc = cmpSentence.toLowerCase();
		}else{
			orgLc = orgSentence;
			cmpLc = cmpSentence;
		}

		orgWords = Arrays.asList(orgLc.split(" "));
		cmpWords = Arrays.asList(cmpLc.split(" "));
		
		for(String s : orgWords){
			String searchWord = "";
			dRet = 0;
			
			for(String ss : cmpWords){
				dRetBuf = getIntelligendWordDistance(s, ss, caseInsensitive);
				if(dRetBuf > dRet){
					searchWord = ss;
					dRet = dRetBuf;
				}
			}
			
			//bestMatches.add(dRet);
			// weight based on search word length
			if(bUseWeightAvarage){
				for(int i = 0; i < s.length(); i++){
					bestMatches.add(dRet);
				}
				for(int i = 0; i < searchWord.length(); i++){
					bestMatches.add(dRet);
				}
			}else{
				bestMatches.add(dRet);
			}
		}

		for(Double d : bestMatches){
			dRetTot = dRetTot + d;
		}

		dRet = (double)(dRetTot / (double)bestMatches.size());

		if(bPrint){
			System.out.println("  distance: " + dRet  + "[" + dRetTot + " / " + bestMatches.size() + "]");
		}
		bSentence = false;
		return dRet;
	}
	
/*	
    public static void main( String[] args )
    {
    	try{
    		double d = 0;
    		FuzzyMatch is = new FuzzyMatch();
    		
    		is.setUseWeightAvarage(true);
    		is.setAllowedExtraLetters(5);
    		is.setPrintCmpStat(true);

    		d = is.getIntelligendSentenceDistance("Downingstreet 10", "Downingstreet 22", true);
 		
    		//d = StringUtils.getJaroWinklerDistance("Yritys keskus Oy", "Oy Yrityskeskus") ;
    		//System.out.println("d: " + d);
    		//d = StringUtils.getLevenshteinDistance("Oy Yritys keskus", "Yrityskeskus Oy") ;
    		//System.out.println("d: " + d);

    		d = is.getIntelligendWordDistance("Michael Kankkonen", "mikael kankonen", true);
    		d = is.getIntelligendWordDistance("zzMichaelxz", "mikael", true, 5);
    		//System.out.println("wordd: " + d);
    		d = is.getIntelligendSentenceDistance("kankkonen", "kankonen", true);
    		d = is.getIntelligendWordDistance("kankkonen", "kankonen", true);
    		d = is.getIntelligendSentenceDistance("Michael", "mikael", true);
    		d = is.getIntelligendSentenceDistance("Michael Kankkonen", "mikael kankonen", true);
    		d = is.getIntelligendWordDistance("Oy yrityskeskus Ab", "Ab Yrityskeskus Oy", true);
    		d = is.getIntelligendWordDistance("Tmi Paulan Kukka", "Kukka Paula", true);
    		
    		d = is.getIntelligendSentenceDistance("Michael Kankkonen", "mikael kankonen", true);
    		d = is.getIntelligendSentenceDistance("Oy yrityskeskus Ab", "Ab Yrityskeskus Oy", true);
    		d = is.getIntelligendSentenceDistance("Oy yoyrityskeskus Ab", "Oy Yoyrityskeskus", true);
    		d = is.getIntelligendSentenceDistance("Stig Michael Kankkonen", "Kankkonen Michael", true);
    		d = is.getIntelligendSentenceDistance("Jhon's Stage", "Stage Jhon", true);
    		d = is.getIntelligendSentenceDistance("Tmi Paulan Kukka", "Kukka Paula", true);
    		d = is.getIntelligendSentenceDistance("Vuokko Kankonen", "Kankkonen Vuokko", true);
    		d = is.getIntelligendSentenceDistance("Mikael Kukkonen", "Kankkonen Michael", true);
    		d = is.getIntelligendSentenceDistance("Pekka Leino", "Kankkonen Michael", true);
    		d = is.getIntelligendSentenceDistance("abc", "defg", true);
    		d = is.getIntelligendSentenceDistance("benz", "audi", true);
    		
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    }
    */
}
